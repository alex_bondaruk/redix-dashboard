$(function() {


});
//Tabs
document.getElementById("defaultOpen").click();
function loadView(evt, name) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(name).style.display = "block";
    evt.currentTarget.className += " active";
}
//Hide
$(".fade").click(function() {
    if ($('.fade').hasClass('active')) {
        $(".sidebar").show(500);
        $(".fade").removeClass('active');
    } else {
        $(".sidebar").hide(500);
        $(".fade").addClass('active');
    }
});

var k = 1024/680;
$(window).resize(function(){
    $('.dashboard__diagram').height($('.dashboard__diagram').width() / k);
});